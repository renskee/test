
package nl.elzerman.birthdaydemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 *
 * @author renskeelzerman
 */
@Controller("/ikbenjarig")
public class BirthdayController {
 
    @GetMapping
    public String showPage(Model model) {
        Quest quest = new Quest();
        model.addAttribute(quest);
        return "birthday";
    } 

    @PostMapping("/trycode")
    public String codeSubmit(@ModelAttribute Quest quest, Model model, RedirectAttributes redirect) {
        if(checkCode(quest.getCode())){
            return "cadeau";
        }
        redirect.addFlashAttribute("message", "Dit is niet goed! Jij moet eerst netjes de speurtocht afronden!");
        return "redirect:birthday";
    }    
    private boolean checkCode(String code) {
        return code.equals("85Oud@0bi9cn!");
    }
}

